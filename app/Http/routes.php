<?php


//use Illuminate\Http\Request;

use Illuminate\Http\Request;

Route::get('/note/insert/{title}/{content}','NoteController@insertNote');
Route::get('/note/find/{id}','NoteController@findNote');
Route::get('/note/fetch','NoteController@fetchNote');
Route::get('/note/delete/{id}','NoteController@deleteNote');
Route::get('/note/update/{id}','NoteController@updateNote');
Route::get('/note/create/{title}/{content}','NoteController@createNote');
Route::get('/note/soft_delete/{id}','NoteController@softDeleteNote');
Route::get('/note/read_soft_delete/{id}','NoteController@readSoftDeleteNote');
Route::get('/note/restore/{id}','NoteController@restoreNote');
Route::get('/note/force_delete/{id}','NoteController@forceDeleteNote');
Route::post('/jsontest', function(Request $request){
    dd($request);
  //  $method = $request->method();
    //$body = $request->body();
    return "";
});
