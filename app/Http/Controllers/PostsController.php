<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "post controller is working";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return "create";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return "show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function editPost()
    {
        return view('edit_post');
    }

    //pass data to view
    public function showPost($id, $name)
    {

        //return view('show_post')->with('id',$id);

        return view('show_post', compact('id', 'name'));
    }

    public function contactPeople()
    {
        $peoples = ['dqwd', 'da', 'dasd'];
        return view('contact', compact('peoples'));
    }

    public function insertPost($title, $sub_title, $content)
    {

        DB::insert('insert into posts(title,sub_title,content) values (?,?,?)', [$title, $sub_title, $content]);

    }

    public function readPost()
    {
        $results = DB::select('select * from posts');

        $res = "<ul>";

        foreach ($results as $result) {
            $res .= "<li>" . $result->title ."</li>";
        }

        $res .= "</ul>";
        return $res;
    }

    public function updatePost()
    {

        $updated = DB::update('update posts set title = "updated title" where id = ?', [1]);
        return $updated;
    }

    public function deletePost()
    {
        $deleted = DB::delete('delete from posts where id=?', [1]);

        return $deleted;
    }
}
