<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;

class NoteController extends Controller
{

    public function insertNote($title, $content)
    {

        $note = new Note;
        $note->title = $title;
        $note->content = $content;
        $note->status = true;

        $note->save();

        return $note;
    }

    public function fetchNote()
    {

        $notes = Note::all();

        return $notes;
    }

    public function findNote($id)
    {
//        $note = Note::find($id);
        $note = Note::findOrFail($id);

        return $note;
    }

    public function deleteNote($id)
    {
        $note = Note::find($id);
        $note->delete();

        return $note;


    }
    public function softDeleteNote($id)
    {
        $note = Note::find($id)->delete();

        return "deleted";
    }
    public function readSoftDeleteNote($id)
    {
        $note = Note::withTrashed()->where('id',$id)->get();

        return $note;
    }
    public function restoreNote($id)
    {
        $note = Note::withTrashed()->where('id',$id)->restore();

        return $note;
    }
    public function forceDeleteNote($id)
    {
        $note = Note::withTrashed()->where('id',$id)->forceDelete();

        return $note;
    }
    public function updateNote($id)
    {
        $note = Note::find($id);

        $note->title = "my xxxxxxxxxxxxx";
        $note->content = "new my xxxxxxxxxx";
        $note->status = true;

        $note->update();
    }

    public function createNote($title, $content)
    {
        $note = Note::create(['title' => $title, 'content' => $content,'status'=>true]);
        return $note;

    }

    public function testJson(Request $request){
        $method = $request->method();
       $body = $request->body();
        return $method." ";
    }

}
